import { useState } from 'react';
import { ToastContainer } from 'react-toastify';

import './App.css';
import 'react-toastify/dist/ReactToastify.css';

import DialogueManager from './components/dialogues/DialogueManager'
import PredicateManager from './components/predicates/PredicateManager';
import Context from './components/context/Context';
import Lexer from './components/predicates/Lexer';
import Parser from './components/predicates/Parser';
import ContextVariables from './components/context/ContextVariables';

function App() {
  const context = new Context(["None", "Happy", "Sad", "Angry", "Scared", "Surprised", "Disgusted"],
    ["Player", "Ambroise", "Clement", "Emile", "Eve", "Gwenaelle", "Inaya"],
    [new ContextVariables(0, "mood", "None"), new ContextVariables(1, "speaker", "Ambroise")]);

  // const lexer = new Lexer("${mood} = None & ${speaker} = Eve");
  // // console.table(lexer.getTokens());
  // const parser = new Parser(lexer);
  // console.log(parser.getPredicate()(context));

  return (
    <>
      <ToastContainer
        position="bottom-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='dark'
      />
      <div className='flex'>
        <DialogueManager
          className='h-screen w-1/2 p-10 flex flex-col justify-start'
          context={context}
        />
        <PredicateManager 
          className='h-screen w-1/2 p-10 flex flex-col justify-start'
          context={context}
        />
      </div>
    </>
  )
}

export default App