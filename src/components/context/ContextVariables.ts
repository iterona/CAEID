class ContextVariables {
    private id = 0;
    private name = "";
    private value = "";

    public getId = () => this.id;
    public getName = () => this.name;
    public getValue = () => this.value;
    
    public setName = (name: string) => this.name = name;
    public setValue = (value: string) => this.value = value;

    constructor(id: number, name: string, value: string) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

}

export default ContextVariables;