import { useState } from "react";
import { toast } from "react-toastify";
import Dialogue from "../dialogues/Dialogue";
import Lexer from "../predicates/Lexer";
import Parser from "../predicates/Parser";
import Predicate from "../predicates/Predicate";
import ContextVariables from "./ContextVariables";

class Context {
    private lexer = new Lexer(this);
    public lex = (value: string) => this.lexer.getTokens(value);

    private parser = new Parser();
    public parse = (value: string) => this.parser.getPredicate(this.lex(value));

    private emotions = [] as string[];
    public getEmotions = () => this.emotions;
    public setEmotions = (emotions: string[]) => {};

    private characters = [] as string[];
    public getCharacters = () => this.characters;
    public setCharacters = (characters: string[]) => {};

    private contextVariables = [] as ContextVariables[];
    public getContextVariables = () => this.contextVariables;
    public setContextVariables = (contextVariables: ContextVariables[]) => {};

    private predicates = [] as Predicate[];
    public getPredicates = () => this.predicates;
    public setPredicates = (predicates: Predicate[]) => {};

    private dialogues = [] as Dialogue[];
    public getDialogues = () => this.dialogues;
    public setDialogues = (dialogues: Dialogue[]) => {};

    constructor(emotions: string[] = ["None"], characters: string[] = [], contextVariables: ContextVariables[] = [], dialogues: Dialogue[] = []) {
        if (emotions.length === 0) {
            emotions = ["None"];
        }

        [this.emotions, this.setEmotions] = useState(emotions as string[]);
        [this.characters, this.setCharacters] = useState(characters as string[]);
        [this.contextVariables, this.setContextVariables] = useState(contextVariables as ContextVariables[]);
        [this.predicates, this.setPredicates] = useState([] as Predicate[]);
        [this.dialogues, this.setDialogues] = useState(dialogues as Dialogue[]);
    }

    // Add an emotion to the list
    // If the emotion already exists, show an error and return false
    // Otherwise, add the emotion and return true
    public addEmotion = (emotion: string) => {
        if (this.emotions.map(e => e.toLowerCase()).includes(emotion.toLowerCase())) {
            toast.error("Emotion already exists!");
            return false;
        }

        this.setEmotions([...this.emotions, emotion]);
        return true;
    }

    // Add a character to the list
    // If the character already exists, show an error and return false
    // Otherwise, add the character and return true
    public addCharacter = (character: string) => {
        if (this.characters.map(c => c.toLowerCase()).includes(character.toLowerCase())) {
            toast.error("Character already exists!");
            return false;
        }

        this.setCharacters([...this.characters, character]);
        return true;
    }

    // Add a dialogue to the list
    public addDialogue = (emotion: string, speaker: string, text: string) => {
        // TODO: maybe find a better way to id the dialogue
        this.setDialogues([...this.dialogues, new Dialogue(this.dialogues.length, emotion, speaker, text)]);
    }
}

export default Context;