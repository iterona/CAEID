import Context from "../context/Context";

class Predicate {
    private id = 0;
    private name = "";
    private cond = {f: () => false, args: []} as {f: (...args: any[]) => boolean, args: any[]};

    public getId = () => this.id;
    public getName = () => this.name;
    public getCond = () => this.cond;

    public setName = (name: string) => this.name = name;
    public setCond(value: string) {
        // let token = [];
        // value.split(" ").forEach(block => {
        //     block.split("").forEach(char => {

        //     }
        // });
    }

    constructor(id: number, name: string, cond: {f: (...args: any[]) => boolean, args: any[]}) {
        this.id = id;
        this.name = name;
        this.cond = cond;
    }

    public evaluate(context: Context) {
        // Get the predicate function
        const f = this.cond.f;
        
        // Get the arguments from the context
        let args = this.cond.args.map(arg => context.getContextVariables().find(cv => cv.getName() === arg)?.getValue());

        // Evaluate the predicate with the curried function and the arguments
        return f(...args);
    };
}

export default Predicate;