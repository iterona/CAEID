import { useState } from "react";

import Context from "../context/Context";

interface PredicateManagerProps {
    className?: string;
    context: Context;
}

const PredicateManager = ({ className, context }: PredicateManagerProps) => {
    return (
        <div className={className}>
            <p>Predicates</p>
        </div>
    );
}

export default PredicateManager;