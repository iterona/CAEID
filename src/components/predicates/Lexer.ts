import { toast } from "react-toastify";
import Context from "../context/Context";

export enum TokenType {
    EQUAL = '=',
    NOT = '!',
    NOT_EQUAL = '!=',
    LESS_THAN = '<',
    LESS_THAN_OR_EQUAL = '<=',
    GREATER_THAN = '>',
    GREATER_THAN_OR_EQUAL = '>=',
    AND = '&',
    OR = '|',
    DOLLAR = '$',
    LCURLY = '{',
    RCURLY = '}',
    VARIABLE = 'VARIABLE',
    VALUE = 'VALUE',
    LPAR = '(',
    RPAR = ')',

    // Parser specific tokens
    OPERATOR = 'OPERATOR',
    PREDICATE = 'PREDICATE',
}

export class Token {
    private type: TokenType;
    private value: string;

    public getType = () => this.type;
    public getValue = () => this.value;

    constructor(type: TokenType, value: string) {
        this.type = type;
        this.value = value;
    }
}

class Lexer {
    private context: Context;
    private tokens: Token[] = [];

    public getTokens(value: string): Token[] {
        try {
            // Return the tokens
            return this.tokenize(value);
        }
        catch (error: any) {
            // Catch any errors, display a toast message and return an empty array
            toast.error(error.message);
            return [];
        }
        finally {
            // Reset the tokens
            this.tokens = [];
        }
    };

    private tokensPeek = (): Token | undefined => this.tokens[0];
    private tokensPop = (): Token | undefined => this.tokens.shift();

    constructor(context: Context) {
        this.context = context;
    }

    private tokenize(value: string): Token[] {
        let contextVariables = this.context.getContextVariables();

        // Transform the value into an array of tokens
        value.split(" ").forEach(block => {
            block.split("").forEach(char => {
                switch (char) {
                    case '=':
                        this.tokens.push(new Token(TokenType.EQUAL, char));
                        break;
                    case '!':
                        this.tokens.push(new Token(TokenType.NOT, char));
                        break;
                    case '<':
                        this.tokens.push(new Token(TokenType.LESS_THAN, char));
                        break;
                    case '>':
                        this.tokens.push(new Token(TokenType.GREATER_THAN, char));
                        break;
                    case '&':
                        this.tokens.push(new Token(TokenType.AND, char));
                        break;
                    case '|':
                        this.tokens.push(new Token(TokenType.OR, char));
                        break;
                    case '$':
                        this.tokens.push(new Token(TokenType.DOLLAR, char));
                        break;
                    case '{':
                        this.tokens.push(new Token(TokenType.LCURLY, char));
                        break;
                    case '}':
                        this.tokens.push(new Token(TokenType.RCURLY, char));
                        break;
                    case '(':
                        this.tokens.push(new Token(TokenType.LPAR, char));
                        break;
                    case ')':
                        this.tokens.push(new Token(TokenType.RPAR, char));
                        break;
                    default:
                        this.tokens.push(new Token(TokenType.VALUE, char));
                        break;
                }
            });
        });

        // Compact the tokens (! and = become !=, etc.)
        // An error will be thrown if the variable token (${...}) is unvalid

        let compactedTokens: Token[] = [];
        let currentToken: Token | undefined;

        while ((currentToken = this.tokensPop()) !== undefined) {
            switch (currentToken.getType()) {
                // compact ! and = into !=
                case TokenType.NOT:
                    if (this.tokensPeek()?.getType() === TokenType.EQUAL) {
                        this.tokensPop();
                        compactedTokens.push(new Token(TokenType.NOT_EQUAL, "!="));
                    }
                    else {
                        throw new Error("Invalid token: " + currentToken.getValue());
                    }
                    break;

                // compact < and = into <=
                case TokenType.LESS_THAN:
                    if (this.tokensPeek()?.getType() === TokenType.EQUAL) {
                        this.tokensPop();
                        compactedTokens.push(new Token(TokenType.LESS_THAN_OR_EQUAL, "<="));
                    }
                    else {
                        compactedTokens.push(currentToken);
                    }
                    break;

                // compact > and = into >=
                case TokenType.GREATER_THAN:
                    if (this.tokensPeek()?.getType() === TokenType.EQUAL) {
                        this.tokensPop();
                        compactedTokens.push(new Token(TokenType.GREATER_THAN_OR_EQUAL, ">="));
                    }
                    else {
                        compactedTokens.push(currentToken);
                    }
                    break;

                // compact ${...} into a variable token
                case TokenType.DOLLAR:
                    if (this.tokensPeek()?.getType() === TokenType.LCURLY) {
                        this.tokensPop();

                        let foundClosingCurly = false;
                        let variableName = "";
        
                        // Loop until we find the closing curly
                        while ((currentToken = this.tokensPop()) !== undefined) {
                            if (currentToken.getType() === TokenType.RCURLY) {
                                foundClosingCurly = true;
                                break;
                            }
                            
                            variableName += currentToken.getValue();
                        }
        
                        // Throw an error if we didn't find the closing curly
                        if (!foundClosingCurly) {
                            throw new Error("Unclosed variable");
                        }

                        // Put the variable name in lowercase
                        variableName = variableName.toLowerCase();

                        // Check if the value is a variable
                        if (contextVariables.find(variable => variable.getName().toLowerCase() === variableName) === undefined) {
                            throw new Error("Invalid variable: " + variableName);
                        }
        
                        // Push the new variable token
                        compactedTokens.push(new Token(TokenType.VARIABLE, variableName));
                    }
                    else {
                        throw new Error("Invalid token: " + currentToken.getValue());
                    }
                    break;

                // throw an error if we find { or } outside of a variable
                case TokenType.LCURLY:
                case TokenType.RCURLY:
                    throw new Error("Invalid token: " + currentToken.getValue());

                // compact value tokens into a single value token
                case TokenType.VALUE:
                    // Get the current token value
                    let value = currentToken.getValue();

                    // While we have value tokens, add them together
                    while ((currentToken = this.tokensPeek()) !== undefined) {
                        if (currentToken.getType() === TokenType.VALUE) {
                            value += currentToken.getValue();
                            this.tokensPop();
                        }
                        else {
                            break;
                        }
                    }

                    // Put the value in lowercase
                    value = value.toLowerCase();

                    // Push the new value token
                    compactedTokens.push(new Token(TokenType.VALUE, value));
                    break;

                default:
                    compactedTokens.push(currentToken);
            }
        }

        return compactedTokens;
    }
}

export default Lexer;