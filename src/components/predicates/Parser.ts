import { toast } from "react-toastify";
import Context from "../context/Context";
import Lexer, { TokenType, Token } from "./Lexer";

class ParserToken extends Token {
    private precedence = 0;
    private f: (...args: any[]) => boolean = () => false;

    public getPrecedence = () => this.precedence;
    public getPredicate = () => this.f;

    constructor(type: TokenType, f: (...args: any[]) => boolean) {
        // Call the superclass constructor with the type and value
        // If the type is PREDICATE, keep it as is, otherwise, set the type to OPERATOR
        // The value is the type of the token
        super(type === TokenType.PREDICATE ? type : TokenType.OPERATOR, type);

        // Set the precedence of the operator
        switch (type) {
            case TokenType.LPAR:
                this.precedence = 3;
                break;
            case TokenType.AND:
                this.precedence = 1;
                break;
            case TokenType.OR:
                this.precedence = 0;
                break;
            default:
                this.precedence = 2;
                break;
        }

        this.f = f;
    }
}

class Parser {
    private tokens = [] as Token[];

    public getPredicate (tokens: Token[]) : (...args: any[]) => boolean {
        this.tokens = tokens;

        // If the tokens are empty, return a false predicate
        if (this.tokens.length === 0) {
            return () => false;
        }

        try {
            // Parse the tokens
            return this.parse();
        }
        catch (error: any) {
            // If an error occurs, display an error and return an false predicate
            toast.error(error.message);
            return () => false;
        }
    };

    // Parse using the shunting-yard algorithm
    private parse() {
        // Create a stack for the operators
        let operators = [] as ParserToken[];

        // Create a stack for the operands
        let operands = [] as Token[];
        
        let token: Token | undefined;
        while ((token = this.tokens.shift()) !== undefined) {
            this.parseToken(token, operands, operators);
        }

        // Check if there are any remaining tokens in the operator stack
        // TODO: IMPLEMENT THE PARENTHESIS CHECK
        if (operators.length > 0) {
            let op: ParserToken | undefined;
            while ((op = operators.pop()) !== undefined) {
                this.popOperator(op, operands);
            }
        }

        // Check if there is only one element in the stack
        if (operands.length !== 1) {
            throw new Error("Invalid expression: could not reduce to a single predicate");
        }

        // Return the predicate function
        return (operands[0] as ParserToken).getPredicate();
    }

    private parseToken(token: Token, operands: Token[], operators: ParserToken[]) {
        let newOperator = undefined;
        switch (token.getType()) {
            // Create the temporary operator
            case TokenType.EQUAL:
                newOperator = new ParserToken(TokenType.EQUAL, (a, b) => a === b);
            case TokenType.NOT_EQUAL:
                newOperator ??= new ParserToken(TokenType.NOT_EQUAL, (a, b) => a !== b);
            case TokenType.LESS_THAN:
                newOperator ??= new ParserToken(TokenType.LESS_THAN, (a, b) => a.localeCompare(b) < 0);
            case TokenType.LESS_THAN_OR_EQUAL:
                newOperator ??= new ParserToken(TokenType.LESS_THAN_OR_EQUAL, (a, b) => a.localeCompare(b) <= 0);
            case TokenType.GREATER_THAN:
                newOperator ??= new ParserToken(TokenType.GREATER_THAN, (a, b) => a.localeCompare(b) > 0);
            case TokenType.GREATER_THAN_OR_EQUAL:
                newOperator ??= new ParserToken(TokenType.GREATER_THAN_OR_EQUAL, (a, b) => a.localeCompare(b) >= 0);
            case TokenType.AND:
                newOperator ??= new ParserToken(TokenType.AND, (a, b) => a && b);
            case TokenType.OR:
                newOperator ??= new ParserToken(TokenType.OR, (a, b) => a || b);

                // Pop the operator stack until the precedence of the new operator is <= to the precedence of the top operator
                // Stop when you find a left parenthesis (precedence 3)
                let op: ParserToken | undefined = operators[operators.length - 1];
                while (op !== undefined && op.getPrecedence() !== 3 && op.getPrecedence() >= newOperator.getPrecedence()) {
                    this.popOperator(operators.pop()!, operands);
                    op = operators[operators.length - 1];
                }

                // Push the new operator to the stack
                operators.push(newOperator);
                newOperator = undefined;
                break;

            // Left parenthesis are pushed to the operator stack
            case TokenType.LPAR:
                operators.push(new ParserToken(TokenType.LPAR, () => true));
                break;

            case TokenType.RPAR:
                let op_: ParserToken | undefined = operators[operators.length - 1];
                let foundMatching = false;

                // Pop the operator stack until the left parenthesis is found
                while ((op_ = operators.pop()) !== undefined) {
                    // If the operator is a left parenthesis (precedence is 3), break
                    if (op_.getPrecedence() === 3) {
                        foundMatching = true;
                        break;
                    }

                    // Otherwise, pop the operator
                    this.popOperator(op_, operands);
                }

                // If the left parenthesis is not found, throw an error
                if (!foundMatching) {
                    throw new Error("Invalid expression: could not find left parenthesis");
                }

                break;

            // Values and variables are pushed to the operands stack
            case TokenType.VALUE:
            case TokenType.VARIABLE:
                operands.push(token);
                break;
        }
    }

    private popOperator(token: ParserToken, operands: Token[]) {
        // Pop the top two elements from the stack
        let op2 = operands.pop();
        let op1 = operands.pop();

        // Check if there are enough operands
        if (op1 === undefined || op2 === undefined) {
            throw new Error("Invalid expression: not enough operands");
        }

        let predicate: ParserToken;

        // Non 2 precedence operators are: and/or
        if (token.getPrecedence() !== 2) {
            // Check if the operands are of the correct type
            if (op1.getType() !== TokenType.PREDICATE || op2.getType() !== TokenType.PREDICATE) {
                throw new Error("Invalid expression : operands must be predicates");
            }

            // Create the predicate function depending on the operator
            predicate = new ParserToken(TokenType.PREDICATE, (context: Context) => {
                let predicate1 = (op1 as ParserToken).getPredicate();
                let predicate2 = (op2 as ParserToken).getPredicate();
                return token.getPredicate()(predicate1(context), predicate2(context));
            });
        }
        // 2 precedence operators are: equal, not equal, less than, less than or equal, greater than, greater than or equal
        else {
            // Check if the operands are of the correct type
            if (op1.getType() !== TokenType.VARIABLE || op2.getType() !== TokenType.VALUE) {
                throw new Error("Invalid expression: operands must be variables and values");
            }

            // Create a new predicate with the temporary predicate function
            predicate = new ParserToken(TokenType.PREDICATE, (context: Context) => {
                let value1 = context.getContextVariables().find(cv => cv.getName() === op1?.getValue())?.getValue();
                return token.getPredicate()(value1?.toLowerCase(), op2?.getValue());
            });
        }

        // Push the predicate to the stack
        operands.push(predicate);
    }
}

export default Parser;