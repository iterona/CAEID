class Dialogue {
    private id = 0;
    private emotion = "";
    private speaker = "";
    private text = "";

    public getId = () => this.id;
    public getEmotion = () => this.emotion;
    public getSpeaker = () => this.speaker;
    public getText = () => this.text;

    public setEmotion = (emotion: string) => this.emotion = emotion;
    public setSpeaker = (speaker: string) => this.speaker = speaker;
    public setText = (text: string) => this.text = text;

    constructor(id: number, emotion: string, speaker: string, text: string) {
        this.id = id;
        this.emotion = emotion;
        this.speaker = speaker;
        this.text = text;
    }
}

export default Dialogue;