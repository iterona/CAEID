import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import Context from "../context/Context";
import { CreatePopUp } from "../popups";
import Dialogue from "./Dialogue";

interface DialogueManagerProps {
    className?: string;
    context: Context
}

let firstRender = [2, 2];
const DialogueManager = ({ className, context }: DialogueManagerProps) => {
    const [displayPopUp, setDisplayPopUp] = useState("");

    // When a new emotion is added, set the current selected emotion to the new emotion
    useEffect(() => {
        const selector = document.getElementById("emotion-selector") as HTMLSelectElement;
        const emotions = context.getEmotions();
        selector.value = emotions[emotions.length - 1];

        // If this is the first render, set the first render to false
        if (firstRender[0]) {
            selector.value = emotions[0];
            firstRender[0]--;
        }
    }, [context.getEmotions()]);

    // When a new character is added, set the current selected character to the new character
    useEffect(() => {
        const selector = document.getElementById("character-selector") as HTMLSelectElement;
        const characters = context.getCharacters();
        selector.value = characters[characters.length - 1] ?? "";

        // If this is the first render, set the first render to false
        if (firstRender[1]) {
            selector.value = "";
            firstRender[1]--;
        }
    }, [context.getCharacters()]);

    // Lookup table for which pop up to display
    const popUpTable = {
        "create-emotion": 
            <CreatePopUp title="Create an emotion"
                        placeholder="Emotion's name"
                        setDisplayPopUp={setDisplayPopUp}
                        setValue={context.addEmotion}/>,
        "create-character": 
            <CreatePopUp title="Create a character"
                        placeholder="Character's name"
                        setDisplayPopUp={setDisplayPopUp}
                        setValue={context.addCharacter}/>,
    } as { [key: string]: any };

    // Update values in the dialogue
    let updateDialogue = (dialogue: Dialogue) => {
        // Get the values from the context
        const emotions = context.getEmotions();
        const characters = context.getCharacters();
        const predicates = context.getPredicates();
        const dialogues = context.getDialogues();

        // Remove < and > from the beggining and end of the emotion if they exist
        dialogue.setEmotion(dialogue.getEmotion().replace(/^<|>$/g, ""));

        // Remove : from the end of the speaker's name if it exists
        dialogue.setSpeaker(dialogue.getSpeaker().replace(/:$/g, ""));

        // Find the dialogue in the list
        let index = dialogues.findIndex(d => d.getId() === dialogue.getId());

        // Get the input fields
        let emotionDiv = document.getElementById(`${dialogue.getId()}-emotion`);
        let characterDiv = document.getElementById(`${dialogue.getId()}-character`);

        // If one of the fields is empty, an error occured, exit the function
        if (emotionDiv === null || characterDiv === null) {
            toast.error("Error updating dialogue! (Could not fetch the input fields)");
            return;
        }

        // If the emotion is not recognized, show an error
        if (!emotions.includes(dialogue.getEmotion())) {
            toast.error("Emotion does not exist!");

            // Reset the emotion
            emotionDiv.innerText = `<${dialogues[index].getEmotion()}>`;
            return;
        }

        // If the character is not recognized, show an error
        if (!characters.includes(dialogue.getSpeaker())) {
            toast.error("Character does not exist!");

            // Reset the character
            characterDiv.innerText = `${dialogues[index].getSpeaker()}:`;
            return;
        }
        
        // If the dialogue changed, show an alert
        if (dialogue.getEmotion() !== dialogues[index].getEmotion() ||
            dialogue.getSpeaker() !== dialogues[index].getSpeaker() ||
            dialogue.getText() !== dialogues[index].getText()) {
            toast.success("Dialogue updated!");
        }

        // If the emotion inputField exists and doesn't start with < and end with >, add them
        if (!emotionDiv.innerText.startsWith("<") || !emotionDiv.innerText.endsWith(">")) {
            emotionDiv.innerText =`<${dialogue.getEmotion()}>`;
        }

        // If the character inputField exists and doesn't end with :, add it
        if (!characterDiv.innerText.endsWith(":")) {
            characterDiv.innerText =  `${dialogue.getSpeaker()}:`;
        }

        // Update the dialogue
        dialogues[index] = dialogue;
        context.setDialogues([...dialogues]);
    }

    // Create a new dialogue
    let createDialogue = (e: any) => {
        e.preventDefault();
    
        // Get the character, if none is selected, show an error
        let selector = document.getElementById("character-selector") as HTMLSelectElement;
        if (selector.selectedIndex === 0) {
            if (!displayPopUp) {
                toast.error("Please select a character!");
            }
            return;
        }

        // Get the dialogue, if none is entered, show an error
        let input = document.getElementById("dialogue-input") as HTMLInputElement;
        if (input.value === "") {
            if (!displayPopUp) {
                toast.error("Please enter a dialogue!");
            }
            return;
        }

        let emotion = document.getElementById("emotion-selector") as HTMLSelectElement;

        // Add the dialogue to the list and clear the input
        context.addDialogue(emotion.value, selector.value, input.value.replace(/\s+/g, ' ').trim());
        input.value = "";
    }

    return (
        <div className={className ?? ""}>
            {displayPopUp !== "" && popUpTable[displayPopUp]}
            <form className="w-full flex flex-col justify-around" onSubmit={createDialogue}>
                <div className="flex justify-between">
                    <select id="emotion-selector" className="w-1/2">
                        {context.getEmotions().map((emotion: string) => {
                            return <option key={emotion} value={emotion}>{emotion}</option>
                        })}
                    </select>
                    <button type="button" onClick={() => setDisplayPopUp("create-emotion")}>Add a new emotion</button>
                </div>
                <div className="flex justify-between">
                    <select id="character-selector" className="w-1/2">
                        <option value="">Select a character</option>
                        {context.getCharacters().map((character: string) => {
                            return <option key={character} value={character}>{character}</option>
                        })}
                    </select>
                    <button type="button" onClick={() => setDisplayPopUp("create-character")}>Add a new character</button>
                </div>
                <div className="flex pt-2 pb-5">
                    <input id="dialogue-input" type="text" placeholder="Type your dialogue here!" className="w-full"/>
                    <button type="submit" className="ml-5">Done!</button>
                </div>
            </form>
            <div className="w-full flex-col justify-around">
                {context.getDialogues().map((dialogue, index) => {
                    return (
                        <div key={index} className="w-full flex-col">
                            <div className="flex">
                                <h1 id={`${dialogue.getId()}-emotion`}
                                    className="mr-2 font-bold"
                                    onBlur={e => updateDialogue(
                                        new Dialogue(dialogue.getId(),
                                            e.target.innerText,
                                            dialogue.getSpeaker(),
                                            dialogue.getText()))}
                                    contentEditable
                                    suppressContentEditableWarning>
                                    {`<${dialogue.getEmotion()}>`}
                                </h1>
                                <h1 id={`${dialogue.getId()}-character`}
                                    className="underline"
                                    onBlur={e => updateDialogue(
                                        new Dialogue(dialogue.getId(),
                                            dialogue.getEmotion(),
                                            e.target.innerText,
                                            dialogue.getText()))}
                                    contentEditable
                                    suppressContentEditableWarning>
                                    {`${dialogue.getSpeaker()}:`}
                                </h1>
                            </div>
                            <h1 className="ml-2"
                                onBlur={e => updateDialogue(
                                    new Dialogue(dialogue.getId(),
                                        dialogue.getEmotion(),
                                        dialogue.getSpeaker(),
                                        e.target.innerText))}
                                contentEditable
                                suppressContentEditableWarning>
                                {dialogue.getText()}
                            </h1>
                        </div>
                    )
                })}
            </div>
        </div>
    )
};

export default DialogueManager;