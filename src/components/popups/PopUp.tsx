import { IoMdClose } from 'react-icons/io';

interface PopUpProps {
    setDisplayPopUp: (display: string) => void;
    content : any;
}

const PopUp = ({ setDisplayPopUp, content } : PopUpProps) => {
    return (
    <div className="absolute h-auto top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 rounded-xl p-10 bg-gray-200">
        <button className="absolute top-0 right-0 mr-5 mt-5" onClick={() => setDisplayPopUp("")}>
            <IoMdClose className="text-3xl"/>
        </button>
        { content }
    </div>
    )
};

export default PopUp;