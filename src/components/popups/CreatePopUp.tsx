import { toast } from "react-toastify";
import PopUp from "./PopUp";

interface CreatePopUpProps {
    title: string;
    placeholder: string;
    setDisplayPopUp: (display: string) => void;
    setValue: (value: string) => boolean;
}

const CreatePopUp = ({ title, placeholder, setDisplayPopUp, setValue }: CreatePopUpProps) => {

    // Get the input and submit button
    const submittHandler = (e: any) => {
        e.preventDefault();

        // Get the input, if none is entered, show an error
        let input = e.target.querySelector("input") as HTMLInputElement;
        if (input.value === "") {
            toast.error("Please enter a value!");
            return;
        }

        // Add the value to the list
        // If the value already exists, return false (the function shows the error)
        if (!setValue(input.value)) {
            return;
        }

        // Close the popup
        setDisplayPopUp("");
    }

    const content = (
        <div className="flex-col items-center">
            <h1>{title}</h1>
            <form className="w-full mt-5 flex justify-around" onSubmit={submittHandler}>
                <input type="text" placeholder={placeholder} className="w-full" autoFocus/>
                <button type="submit" className="ml-5">Done!</button>
            </form>
        </div>
    )

    return (
        <PopUp setDisplayPopUp={setDisplayPopUp} content={content}/>
    );
}

export default CreatePopUp;