# CAEID: Context Aware Explicit Implicit Dialogues

CAEID is a web app project to aid in the writing of context aware and explicit
dialogues. It is a sub-project of the half narrative game `ITERONA`
Implicit choice will come later in the game

Context aware indicates that the dialogue flow will be influenced by the context
Every single choice the player makes is saved in the context
Example of context saved (in addition to every choice the player picked):
- time taken to answer
- current state of conversation (friendly, angry, sad, etc…)
- level of such state (high, medium, low)
- who is present in the conversation
- who is the latest person that talked and what did they say
- etc…

CAEID creates a huge data base of dialogues, each having a set of predicates 
matching the context. The dialogue selected is the one whose rule (predicate) 
is the most precise. In case of equality, pick one at random.

## For example
In a dialogue with the player, Ambroise and Eve:
When a dialogue is triggered, if Ambroise said "hello" and the database is:

| SPEAKER | DIALOGUE | RULES |
|---------|----------|-------|
| Any | "hello" | None |
| Eve | "I'm good and you?" | last dialogue is "how are you?" |
| Eve | "Hi Ambroise!" | last dialogue is "hello" and last speaker is Ambroise and mood is happy |
| Eve | "Hi!" | last dialogue is "hello" and mood is happy |

and the mood is happy, then the dialogue chosen will be "Hi Ambroise!"

## Getting started

Install the dependencies:
```
npm i
```

To run a dev version on localhost:
```
npm run dev
```

To build the app:
```
# This will create or update the dist/ folder
npm run build
```
